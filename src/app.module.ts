import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEmitterModule } from '@nestjs/event-emitter';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { configService } from './services/config.service';
import { UsersModule } from './users/users.module';
import { PartiesModule } from './parties/parties.module';
import { PartyUserModule } from './party-users/party-users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    EventEmitterModule.forRoot(),
    UsersModule,
    PartiesModule,
    PartyUserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
