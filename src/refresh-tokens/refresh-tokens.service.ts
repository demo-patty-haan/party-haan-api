import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import moment = require('moment');
import { randomBytes } from 'crypto';

import { RefreshTokenReqDto } from './dto/refresh-token-req.dto';
import { RefreshToken } from './entities/refresh-token.entity';

@Injectable()
export class RefreshTokensService {
  constructor(
    @InjectRepository(RefreshToken)
    private readonly refreshTokensRepository: Repository<RefreshToken>,
  ) {}

  create(username: string): Promise<RefreshToken> {
    const refreshToken = new RefreshToken();
    refreshToken.username = username;
    refreshToken.refresh_token = randomBytes(64).toString('hex');
    refreshToken.valid = moment().add(6, 'months').toDate();
    return this.refreshTokensRepository.save(refreshToken);
  }

  async exchangeToken(
    refreshTokenReqDto: RefreshTokenReqDto,
  ): Promise<RefreshToken> {
    const refreshToken: RefreshToken =
      await this.refreshTokensRepository.findOne({
        where: {
          refresh_token: refreshTokenReqDto.refresh_token,
        },
      });
    if (refreshToken === undefined || refreshToken.valid < moment().toDate()) {
      return undefined;
    }
    return refreshToken;
  }
}
