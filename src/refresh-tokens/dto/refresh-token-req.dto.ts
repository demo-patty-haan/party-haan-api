import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RefreshTokenReqDto {
  @ApiProperty({ example: 'abc123', description: 'Refresh token' })
  @IsString()
  @IsNotEmpty()
  refresh_token: string;
}
