import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RefreshTokenResDto {
  @ApiProperty({ example: 'abc123', description: 'Refresh token' })
  @IsString()
  @IsNotEmpty()
  refresh_token: string;

  @ApiProperty({ example: 'abc123', description: 'Jwt token' })
  @IsString()
  @IsNotEmpty()
  token: string;
}
