import { IsNotEmpty, IsNumber, Allow } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { IsUnique2 } from './../../validators/is-unique2.validator';
import { IsPartyNotFull } from './../../validators/is-party-not-full.validator';

export class CreatePartyUserDto {
  @Allow()
  context?: {
    params: any;
    query: any;
    user: any;
  };

  @ApiProperty({ example: 1, description: 'party_id' })
  @IsNumber()
  @IsNotEmpty()
  @IsUnique2(
    { table: 'party_user', column: 'party_id' },
    { message: 'party user already exists' },
  )
  @IsPartyNotFull({ message: 'party is full' })
  party_id: number;
}
