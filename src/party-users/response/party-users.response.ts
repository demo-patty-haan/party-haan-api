import { ApiProperty } from '@nestjs/swagger';

import { PartyUser } from '../entities/party-user.entity';

export class PartiesResponse {
  @ApiProperty({ type: [PartyUser] })
  party_users: Record<string, any>[];

  @ApiProperty()
  total: number;
}
