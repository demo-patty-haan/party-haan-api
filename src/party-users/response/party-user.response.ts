import { ApiProperty } from '@nestjs/swagger';

import { PartyUser } from '../entities/party-user.entity';

export class PartyUserResponse {
  @ApiProperty()
  party_user: PartyUser;
}
