import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';

import { PartyUserInsertedDeletedEvent } from '../events/party-user-inserted-deleted.event';
import { PartyUsersService } from '../party-users.service';
import { PartiesService } from '../../parties/parties.service';

@Injectable()
export class PartyUserInsertedDeletedListener {
  constructor(
    private readonly partyUsersService: PartyUsersService,
    private readonly partiesService: PartiesService,
  ) {}

  @OnEvent('party-user.inserted-deleted')
  async handleOrderCreatedEvent(event: PartyUserInsertedDeletedEvent) {
    const total = await this.partyUsersService.countUserInParty({
      partyId: event.partyId,
    });
    console.log(`${total} --- ${event.partyId}`);
    await this.partiesService.updateTotal({ total: total, id: event.partyId });
  }
}
