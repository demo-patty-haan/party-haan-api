import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Unique,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

import { Party } from '../../parties/entities/party.entity';
import { User } from '../../users/entities/user.entity';

@Entity()
@Unique('user_party_index', ['user', 'party'])
export class PartyUser {
  @PrimaryGeneratedColumn()
  @ApiProperty({ example: '1', description: 'Id' })
  id: number;

  @CreateDateColumn({ name: 'created' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Creation date',
  })
  created: Date;

  @UpdateDateColumn({ name: 'updated' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Update date',
  })
  updated: Date;

  @ApiProperty({
    type: Party,
  })
  @ManyToOne(() => Party, (party) => party.party_users)
  @JoinColumn({ name: 'party_id' })
  party: Party;

  @ManyToOne(() => User, (user) => user.party_users)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
