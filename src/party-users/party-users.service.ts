import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { PartyUser } from './entities/party-user.entity';
import { CreatePartyUserDto } from './dto/create-party-user.dto';
import { Party } from '../parties/entities/party.entity';
import { User } from '../users/entities/user.entity';
import { PartyUserInsertedDeletedEvent } from './events/party-user-inserted-deleted.event';

@Injectable()
export class PartyUsersService {
  constructor(
    @InjectRepository(PartyUser)
    private readonly partyUserRepository: Repository<PartyUser>,
    private eventEmitter: EventEmitter2,
  ) {}

  async create(
    createPartyUserDto: CreatePartyUserDto,
    userId: number,
  ): Promise<PartyUser> {
    const party = new Party();
    party.id = createPartyUserDto.party_id;

    const user = new User();
    user.id = userId;

    const partyUser = new PartyUser();
    partyUser.party = party;
    partyUser.user = user;

    const partyUser2 = await this.partyUserRepository.save(partyUser);

    const partyUserInsertedDeletedEvent = new PartyUserInsertedDeletedEvent();
    partyUserInsertedDeletedEvent.partyId = party.id;

    this.eventEmitter.emit(
      'party-user.inserted-deleted',
      partyUserInsertedDeletedEvent,
    );

    return partyUser2;
  }

  findAndCount({
    page = 1,
    limit = 30,
    partyId,
    userId,
  }: {
    page?: number;
    limit?: number;
    partyId: number;
    userId: number;
  }) {
    const where = {};

    if (partyId) {
      where['party'] = {
        id: partyId,
      };
    }

    if (userId) {
      where['user'] = {
        id: userId,
      };
    }

    return this.partyUserRepository.findAndCount({
      where: where,
      take: limit,
      skip: (page - 1) * limit,
      order: { updated: 'DESC' },
      relations: ['party', 'user'],
    });
  }

  async deleteById(id: number) {
    const partyUser = await this.partyUserRepository.findOneOrFail(id, {
      relations: ['party'],
    });
    const partyId = partyUser.party.id;

    await this.partyUserRepository.delete(id);

    const partyUserInsertedDeletedEvent = new PartyUserInsertedDeletedEvent();
    partyUserInsertedDeletedEvent.partyId = partyId;

    this.eventEmitter.emit(
      'party-user.inserted-deleted',
      partyUserInsertedDeletedEvent,
    );
  }

  async countUserInParty({ partyId }: { partyId: number }): Promise<number> {
    return await this.partyUserRepository.count({
      where: {
        party: {
          id: partyId,
        },
      },
    });
  }
}
