import {
  Controller,
  Get,
  Query,
  Post,
  Delete,
  Body,
  Request,
  Param,
  UseInterceptors,
  ClassSerializerInterceptor,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiQuery,
} from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';

import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { PartyUser } from './entities/party-user.entity';
import { PartyUsersService } from './party-users.service';
import { CreatePartyUserDto } from './dto/create-party-user.dto';
import { PartyUserResponse } from './response/party-user.response';
import { PartiesResponse } from './response/party-users.response';
import { DeleteResult } from 'typeorm';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth('JWT')
@Controller()
@ApiTags('Party User')
export class PartyUsersController {
  constructor(private readonly partyUsersService: PartyUsersService) {}

  @Get('auth/v1/party_users')
  @ApiOperation({ summary: 'Get party users' })
  @ApiResponse({
    status: 200,
    type: PartiesResponse,
  })
  @ApiQuery({ name: 'page', required: false })
  @ApiQuery({ name: 'limit', required: false })
  @ApiQuery({ name: 'party_id', required: false })
  @ApiQuery({ name: 'user_id', required: false })
  async getPartyUsers(
    @Request() req,
    @Query('party_id') party_id?: number,
    @Query('user_id') user_id?: number,
    @Query('page') page?: number,
    @Query('limit') limit?: number,
  ): Promise<PartiesResponse> {
    const [partyUsers, total] = await this.partyUsersService.findAndCount({
      page: page,
      limit: limit,
      partyId: party_id,
      userId: user_id,
    });

    return {
      party_users: partyUsers.map((partyUser) => classToPlain(partyUser)),
      total: total,
    };
  }

  @Post('auth/v1/party_user')
  @ApiOperation({ summary: 'Create new party user' })
  @ApiResponse({
    status: 201,
    type: PartyUserResponse,
  })
  async createPartyUser(
    @Request() req,
    @Body() createPartyUserDto: CreatePartyUserDto,
  ): Promise<PartyUserResponse> {
    return {
      party_user: await this.partyUsersService.create(
        createPartyUserDto,
        req.user.userId,
      ),
    };
  }

  @Delete('auth/v1/party_user/:id')
  @ApiOperation({ summary: 'Delete party user' })
  @ApiResponse({
    status: 200,
  })
  async deletePartyUser(@Param('id') id: number): Promise<void> {
    await this.partyUsersService.deleteById(id);
  }
}
