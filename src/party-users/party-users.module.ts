import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PartyUser } from './entities/party-user.entity';
import { PartyUsersService } from './party-users.service';
import { PartyUsersController } from './party-users.controller';
import { PartyUserInsertedDeletedListener } from './listeners/party-user-inserted-deleted.listener';
import { PartiesModule } from '../parties/parties.module';

@Module({
  imports: [TypeOrmModule.forFeature([PartyUser]), PartiesModule],
  controllers: [PartyUsersController],
  providers: [PartyUsersService, PartyUserInsertedDeletedListener],
})
export class PartyUserModule {}
