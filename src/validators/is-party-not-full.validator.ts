import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import { getManager } from 'typeorm';

@ValidatorConstraint({ async: true })
export class isPartyNotFullValidator implements ValidatorConstraintInterface {
  validate(columnNameValue: any) {
    return getManager()
      .query(`SELECT * FROM party WHERE id = '${columnNameValue}' LIMIT 1`)
      .then((party) => {
        if (party[0]['total_users'] >= party[0]['max_total_users'])
          return false;
        return true;
      });
  }
}
export function IsPartyNotFull(validationOptions?: ValidationOptions) {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: isPartyNotFullValidator,
    });
  };
}
