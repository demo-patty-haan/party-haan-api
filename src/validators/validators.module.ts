import { isUniqueValidator } from './is-unique.validator';
import { isUnique2Validator } from './is-unique2.validator';
import { isPartyNotFullValidator } from './is-party-not-full.validator';
import { Module } from '@nestjs/common';

@Module({
  providers: [isUniqueValidator, isUnique2Validator, isPartyNotFullValidator],
})
export class ValidatorsModule {}
