import {
  Controller,
  Get,
  Query,
  Post,
  Body,
  Request,
  Param,
  UseInterceptors,
  ClassSerializerInterceptor,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiQuery,
} from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';

import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { Party } from './entities/party.entity';
import { PartiesService } from './parties.service';
import { PartiesResponse } from './response/parties.response';
import { CreatePartyDto } from './dto/create-party.dto';
import { PartyResponse } from './response/party.response';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth('JWT')
@Controller()
@ApiTags('Party')
export class PartiesController {
  constructor(private readonly partiesService: PartiesService) {}

  @ApiOperation({ summary: 'Get parties' })
  @ApiResponse({
    status: 200,
    description: 'The found records',
    type: PartiesResponse,
  })
  @ApiQuery({ name: 'page', required: false })
  @ApiQuery({ name: 'limit', required: false })
  @Get('auth/v1/parties')
  async getParties(
    @Request() req,
    @Query('page') page?: number,
    @Query('limit') limit?: number,
  ): Promise<PartiesResponse> {
    const [parties, total] = await this.partiesService.findAndCount({
      page: page,
      limit: limit,
    });

    parties.forEach((party) => {
      party.is_joined =
        party.party_users.findIndex(
          (party_user) => party_user.user.id == req.user.userId,
        ) != -1;
    });

    return {
      parties: parties.map((party) => classToPlain(party)),
      total: total,
    };
  }

  @ApiOperation({ summary: 'Get party' })
  @ApiResponse({
    status: 200,
    description: 'The found party',
    type: PartyResponse,
  })
  @Get('auth/v1/party/:id')
  async getParty(
    @Param('id') id: number,
    @Request() req,
  ): Promise<PartyResponse> {
    const party = await this.partiesService.findById(id);
    party.is_joined =
      party.party_users.findIndex(
        (party_user) => party_user.user.id == req.user.userId,
      ) != -1;
    return {
      party: classToPlain(party),
    };
  }

  @Post('auth/v1/party')
  @ApiOperation({ summary: 'Create new party' })
  @ApiResponse({
    status: 201,
    type: PartyResponse,
  })
  async createParty(
    @Body() createPartyDto: CreatePartyDto,
    @Request() req,
  ): Promise<PartyResponse> {
    return {
      party: await this.partiesService.create(createPartyDto, req.user.userId),
    };
  }
}
