import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreatePartyDto {
  @ApiProperty({ example: 'หน้ากากอนามัย 30 ห่อ', description: 'name' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: '5', description: 'max total users' })
  @IsNumber()
  @IsNotEmpty()
  max_total_users: number;
}
