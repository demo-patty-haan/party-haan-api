import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
  AfterLoad,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

import { PartyUser } from '../../party-users/entities/party-user.entity';
import { User } from '../../users/entities/user.entity';

@Entity()
export class Party {
  @PrimaryGeneratedColumn()
  @ApiProperty({ example: '1', description: 'Id' })
  id: number;

  @CreateDateColumn({ name: 'created' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Creation date',
  })
  created: Date;

  @UpdateDateColumn({ name: 'updated' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Update date',
  })
  updated: Date;

  @Column({ type: 'text' })
  @ApiProperty({ example: 'หน้ากากอนามัย 30 ห่อ', description: 'ชื่อปาร์ตี้' })
  name: string;

  @Column()
  @ApiProperty({ example: 5, description: 'จำนวนคนที่ขาด' })
  max_total_users: number;

  @Column({ default: 0 })
  @ApiProperty({ example: 1, description: 'จำนวนคนที่เข้าร่วม' })
  total_users: number;

  @OneToMany(() => PartyUser, (party_user) => party_user.party)
  @Exclude()
  party_users: PartyUser[];

  @ApiProperty({ example: true, description: 'join or not' })
  is_joined: boolean;

  @ApiProperty({
    type: Party,
  })
  @ManyToOne(() => User, (user) => user.parties)
  @JoinColumn({ name: 'user_id' })
  creator: User;
}
