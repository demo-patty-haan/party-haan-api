import { ApiProperty } from '@nestjs/swagger';

import { Party } from '../entities/party.entity';

export class PartyResponse {
  @ApiProperty({ type: Party })
  party: Record<string, any>;
}
