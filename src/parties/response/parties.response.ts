import { ApiProperty } from '@nestjs/swagger';

import { Party } from '../entities/party.entity';

export class PartiesResponse {
  @ApiProperty({ type: [Party] })
  parties: Record<string, any>[];

  @ApiProperty()
  total: number;
}
