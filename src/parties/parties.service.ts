import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';

import { Party } from './entities/party.entity';
import { User } from '../users/entities/user.entity';
import { CreatePartyDto } from './dto/create-party.dto';

@Injectable()
export class PartiesService {
  constructor(
    @InjectRepository(Party)
    private readonly partyRepository: Repository<Party>,
  ) {}

  findAndCount({ page = 1, limit = 30 }: { page?: number; limit?: number }) {
    return this.partyRepository.findAndCount({
      relations: ['party_users', 'party_users.user'],
      take: limit,
      skip: (page - 1) * limit,
      order: { updated: 'DESC' },
    });
  }

  findById(id: number) {
    return this.partyRepository.findOneOrFail(id, {
      relations: ['party_users', 'party_users.user'],
    });
  }

  create(createUserDto: CreatePartyDto, userId: number): Promise<Party> {
    const party = new Party();
    party.max_total_users = createUserDto.max_total_users;
    party.name = createUserDto.name;
    const user = new User();
    user.id = userId;
    party.creator = user;

    return this.partyRepository.save(party);
  }

  async updateTotal({
    total,
    id,
  }: {
    total: number;
    id: number;
  }): Promise<UpdateResult> {
    const party = await this.partyRepository.findOneOrFail(id, {
      relations: ['creator'],
    });
    party.total_users = total;
    return this.partyRepository.update({ id: id }, { total_users: total });
  }
}
