import { IsString, IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { IsUnique } from './../../validators/is-unique.validator';

export class CreateUserDto {
  @ApiProperty({ example: 'doctor', description: 'Username' })
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @IsUnique(
    { table: 'user', column: 'username' },
    { message: 'Username $value already exists' },
  )
  username: string;

  @ApiProperty({ example: '123456', description: 'Password' })
  @IsString()
  @IsNotEmpty()
  password: string;
}
