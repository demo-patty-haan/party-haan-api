import {
  Controller,
  Get,
  Post,
  Body,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';

import { User } from './entities/user.entity';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { RefreshTokenResDto } from '../refresh-tokens/dto/refresh-token-res.dto';
import { RefreshTokenReqDto } from '../refresh-tokens/dto/refresh-token-req.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { UserResponse } from './response/user.response';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('public/v1/user')
  @ApiOperation({ summary: 'Register (create) new user' })
  @ApiTags('User')
  @ApiResponse({
    status: 201,
    type: UserResponse,
  })
  async create(@Body() createUserDto: CreateUserDto): Promise<UserResponse> {
    return {
      user: classToPlain(await this.usersService.create(createUserDto)),
    };
  }

  @Post('public/v1/login_check')
  @ApiOperation({ summary: 'Login' })
  @ApiTags('Authentication')
  @ApiResponse({
    status: 201,
    type: RefreshTokenResDto,
  })
  async login(@Body() loginUserDto: LoginUserDto): Promise<RefreshTokenResDto> {
    return await this.usersService.login(loginUserDto);
  }

  @Post('public/v1/token_refresh')
  @ApiOperation({ summary: 'Exchange jwt token with refresh token' })
  @ApiTags('Authentication')
  @ApiResponse({
    status: 201,
    description: 'The found record',
    type: RefreshTokenResDto,
  })
  async exchangeToken(
    @Body() refreshTokenDto: RefreshTokenReqDto,
  ): Promise<RefreshTokenResDto> {
    return await this.usersService.exchangeToken(refreshTokenDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT')
  @ApiOperation({ summary: 'Get current user profile' })
  @ApiTags('User')
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: UserResponse,
  })
  @Get('auth/v1/user/me')
  async getUser(@Request() req): Promise<UserResponse> {
    return {
      user: classToPlain(await this.usersService.findById(req.user.userId)),
    };
  }
}
