import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './entities/user.entity';
import { UsersService } from './users.service';
import { AuthModule } from '../auth/auth.module';
import { RefreshTokensModule } from '../refresh-tokens/refresh-tokens.module';
import { UsersController } from './users.controller';

@Module({
  imports: [TypeOrmModule.forFeature([User]), AuthModule, RefreshTokensModule],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
