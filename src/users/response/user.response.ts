import { ApiProperty } from '@nestjs/swagger';

import { User } from '../entities/user.entity';

export class UserResponse {
  @ApiProperty({ type: User })
  user: Record<string, any>;
}
