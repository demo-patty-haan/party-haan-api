import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { hashSync } from 'bcrypt';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { RefreshToken } from '../refresh-tokens/entities/refresh-token.entity';
import { LoginUserDto } from './dto/login-user.dto';
import { RefreshTokenResDto } from '../refresh-tokens/dto/refresh-token-res.dto';
import { RefreshTokenReqDto } from '../refresh-tokens/dto/refresh-token-req.dto';
import { RefreshTokensService } from '../refresh-tokens/refresh-tokens.service';
import { AuthService } from './../auth/auth.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly refreshTokensService: RefreshTokensService,
    private readonly authService: AuthService,
  ) {}

  create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.username = createUserDto.username;
    user.password = hashSync(createUserDto.password, 10);
    user.roles = [User.ROLE_USER];
    user.type = User.TYPE_USER;

    return this.usersRepository.save(user);
  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  findByUsername(username: string): Promise<User> {
    return this.usersRepository.findOne({ where: { username: username } });
  }

  findById(id: string): Promise<User> {
    return this.usersRepository.findOne({ where: { id: id } });
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }

  async login(loginUserDto: LoginUserDto): Promise<RefreshTokenResDto> {
    let user: User = await this.findByUsername(loginUserDto.username);
    if (!user) {
      throw new UnauthorizedException();
    }

    user = await this.authService.validateUser(user, loginUserDto.password);
    if (!user) {
      throw new UnauthorizedException();
    }
    const token: string = await this.authService.generateJwt(user);
    const refreshToken: RefreshToken = await this.refreshTokensService.create(
      user.username,
    );

    return {
      token: token,
      refresh_token: refreshToken.refresh_token,
    };
  }

  async exchangeToken(
    refreshTokenDto: RefreshTokenReqDto,
  ): Promise<RefreshTokenResDto> {
    const refreshToken: RefreshToken =
      await this.refreshTokensService.exchangeToken(refreshTokenDto);
    if (!refreshToken) {
      throw new UnauthorizedException();
    }

    const user: User = await this.findByUsername(refreshToken.username);
    if (user === undefined) {
      throw new UnauthorizedException();
    }

    const token: string = await this.authService.generateJwt(user);
    return {
      token: token,
      refresh_token: refreshToken.refresh_token,
    };
  }
}
