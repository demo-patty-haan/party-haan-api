import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { PartyUser } from '../../party-users/entities/party-user.entity';
import { Party } from '../../parties/entities/party.entity';

@Entity()
export class User {
  static readonly ROLE_ADMIN = 'ROLE_ADMIN';
  static readonly ROLE_USER = 'ROLE_USER';

  static readonly TYPE_USER = 'TYPE_USER';
  static readonly TYPE_ADMIN = 'TYPE_ADMIN';

  @PrimaryGeneratedColumn()
  @ApiProperty({ example: '1', description: 'Id' })
  id: number;

  @CreateDateColumn({ name: 'created' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Creation date',
  })
  created: Date;

  @UpdateDateColumn({ name: 'updated' })
  @ApiProperty({
    example: '2021-05-31T14:41:33.652Z',
    description: 'Update date',
  })
  updated: Date;

  @Column()
  @ApiProperty({ example: 'guard', description: 'Username' })
  username: string;

  @Column()
  @Exclude()
  password: string;

  @Column('json')
  @ApiProperty({ example: '', description: 'Roles' })
  roles: string[];

  @Column({ nullable: true })
  @ApiProperty({ example: 'user', description: 'Type' })
  type: string;

  @OneToMany(() => PartyUser, (party_user) => party_user.user)
  party_users: PartyUser[];

  @OneToMany(() => Party, (party) => party.creator)
  parties: Party[];
}
