import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

class ConfigService {
  private readonly envConfig: Record<string, string>;

  constructor(filePath: string) {
    this.envConfig = dotenv.parse(fs.readFileSync(filePath));
  }

  get(key: string): string {
    return this.envConfig[key];
  }

  getBoolean(key: string): boolean {
    return this.envConfig[key] == 'true';
  }

  getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      host: this.get('DB_HOST'),
      port: +this.get('DB_PORT'),
      username: this.get('DB_USERNAME'),
      password: this.get('DB_PASSWORD'),
      database: this.get('DB_DATABASE'),
      entities: [this.get('DB_ENTITIES')],
      migrationsTableName: 'migrations',
      migrations: ['dist/migration/*.js'],
      cli: { migrationsDir: 'migration' },

      synchronize: this.getBoolean('DB_SYNCHRONIZE'),
      logging: this.getBoolean('DB_LOGGING'),
      autoLoadEntities: true,
    };
  }
}

const configService = new ConfigService(
  `${process.env.NODE_ENV || 'development'}.env`,
);

export { configService };
